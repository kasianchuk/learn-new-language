class AddGroupToLessons < ActiveRecord::Migration[6.1]
  def change
    add_reference :lessons, :group, index: true, foreign_key: true
  end
end

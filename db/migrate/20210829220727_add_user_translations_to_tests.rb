class AddUserTranslationsToTests < ActiveRecord::Migration[6.1]
  def change
    add_column :tests, :user_translations, :text
  end
end

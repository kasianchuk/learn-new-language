class CreateTests < ActiveRecord::Migration[6.1]
  def change
    create_table :tests do |t|
      t.integer :lesson_id
      t.integer :user_id
      t.integer :mark

      t.timestamps
    end
  end
end

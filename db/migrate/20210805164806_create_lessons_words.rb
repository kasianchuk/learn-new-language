class CreateLessonsWords < ActiveRecord::Migration[6.1]
  def change
    create_table :lessons_words do |t|
      t.belongs_to :lesson, null: false, foreign_key: true
      t.belongs_to :word, null: false, foreign_key: true

      t.timestamps
    end
  end
end

class AddTranslationToWords < ActiveRecord::Migration[6.1]
  def change
    add_column :words, :translation, :string
  end
end

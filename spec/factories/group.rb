FactoryBot.define do
  factory :group, class: 'Group' do
    title { 'Group' }
  end
end

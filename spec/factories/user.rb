FactoryBot.define do
  factory :user, class: 'User' do
    first_name { 'John' }
    last_name  { 'Doe' }
    role { 0 }
    email { 'mail@mail.com' }
    password { '123456' }
  end
end

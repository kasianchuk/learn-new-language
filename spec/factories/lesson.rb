FactoryBot.define do
  factory :lesson, class: 'Lesson' do
    title { 'Lessons title' }
  end
end

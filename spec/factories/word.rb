FactoryBot.define do
  factory :word do
    word { 'cat' }
    description { 'animal' }
    translation { 'Кіт' }
  end
end

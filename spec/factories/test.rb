FactoryBot.define do
  factory :test, class: 'Test' do
    lesson_id { 1 }
    user_id { 1 }
    mark { 10 }
    user_translations { '{"1" => "Кіт"}' }
  end
end

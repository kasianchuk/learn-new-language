require 'rails_helper'

RSpec.describe(LessonsController, type: :controller) do
  describe 'GET new' do
    it 'has a 200 status code' do
      get :new
      expect(response.status).to(eq(200))
    end
  end

  describe 'POST create' do
    let!(:group) { create(:group) }

    it 'successfully creates a new lesson' do
      expect do
        post(:create, params: { lesson: { title: 'Title', group_id: group.id } })
      end.to(change(Lesson, :count).by(1))
      expect(response).to(have_http_status(:success))
    end
  end
end

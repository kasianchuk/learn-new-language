require 'rails_helper'

RSpec.describe(LessonsWordsController, type: :controller) do
  describe 'GET new' do
    it 'has a 200 status code' do
      get :new
      expect(response.status).to(eq(200))
    end
  end

  describe 'POST create' do
    let(:group) { create(:group) }
    let(:word) { create(:word) }
    let(:lesson) { create(:lesson, group: group) }
    let(:lessons_word) { LessonsWord.create(word_id: word.id, lesson_id: lesson.id) }

    it 'successfully adds word to lesson' do
      expect do
        post(:create, params: { lessons_word: { word_id: word.id, lesson_id: lesson.id } })
      end.to(change(LessonsWord, :count).by(1))
    end
  end
end

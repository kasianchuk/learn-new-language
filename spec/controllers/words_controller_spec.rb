require 'rails_helper'

RSpec.describe(WordsController, type: :controller) do
  let(:word) { Word.create(word: 'cat', description: 'animal') }

  describe 'GET index' do
    it 'has a 200 status code' do
      get :index
      expect(response.status).to(eq(200))
    end
  end

  describe 'GET show' do
    it 'has a 200 status code' do
      get :show, params: { id: word.id }
      expect(response.status).to(eq(200))
    end
  end

  describe 'GET new' do
    it 'has a 200 status code' do
      get :new
      expect(response.status).to(eq(200))
    end
  end

  describe 'POST create' do
    it 'successfully creates a new word' do
      expect do
        post(:create, params: { word: { word: 'milk', description: 'drink' } })
      end.to(change(Word, :count).by(1))
    end
  end

  describe 'GET edit' do
    it 'has a 200 status code' do
      get :edit, params: { id: word.id }
      expect(response.status).to(eq(200))
    end
  end

  describe 'PUT update' do
    let(:word_params) { { word: 'dog', description: 'pet' } }

    it 'successfully updated' do
      put :update, params: { id: word.id, word: word_params }
      word.reload

      expect(word.word).to(eql(word_params[:word]))
      expect(word.description).to(eql(word_params[:description]))
    end
  end

  describe 'DELETE destroy' do
    it 'destroy word' do
      word.save
      expect do
        delete(:destroy, params: { id: word.id })
      end.to(change(Word, :count).by(-1))
    end
  end
end

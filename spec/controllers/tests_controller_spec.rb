require 'rails_helper'

RSpec.describe(TestsController, type: :controller) do
  let(:user) { create(:user) }

  before do
    sign_in user
  end

  describe 'GET new' do
    it 'has a 200 status code' do
      get :new
      expect(response.status).to(eq(200))
    end
  end

  describe 'POST create' do
    let(:group) { create(:group) }
    let(:word) { create(:word) }
    let(:lesson) { create(:lesson, group_id: group.id) }
    let(:test) { create(:test) }

    it 'successfully creates a new test' do
      Lesson.find(lesson.id).words.create(word: 'Cat', description: 'I am a cat',
                                          translation: 'Кіт')
      expect do
        post(:create,
             params: { lesson_id: lesson.id, user_id: user.id, user_translations: { '1': 'Кіт' } })
      end.to(change(Test, :count).by(1))
    end
  end
end

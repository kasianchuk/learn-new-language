# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Word, type: :model do
  context 'when word has relations' do
    subject(:word) { described_class.new }

    it { expect(word).to(have_many(:lessons)) }
  end

  context 'when validations present' do
    subject(:word) { described_class.new }

    it 'is valid with valid attributes' do
      word.word        = 'Word'
      word.description = 'Description'
      expect(word).to be_valid
    end

    it 'is not valid without attribute word' do
      word.word = ''
      expect(word).not_to be_valid
    end

    it 'is not valid without attribute description' do
      word.description = ''
      expect(word).not_to be_valid
    end
  end
end

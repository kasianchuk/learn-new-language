# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Lesson, type: :model do
  subject(:lesson) { described_class.new }

  let(:group) { create(:group) }

  context 'relations' do
    it { expect(lesson).to(have_many(:words)) }
  end

  context 'when validations present' do
    subject(:lesson) { described_class.new }

    it 'is valid with valid attributes' do
      lesson.title    = 'Anything'
      lesson.group_id = group.id
      expect(lesson).to(be_valid)
    end

    it 'is not valid without attributes' do
      lesson.title = ''
      expect(lesson).not_to be_valid
    end
  end
end

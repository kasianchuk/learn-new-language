require 'rails_helper'

RSpec.describe(Group, type: :model) do
  describe 'factory' do
    it { expect(described_class.new(title: 'new title')).to(be_valid) }
    it { expect(described_class.new(title: nil)).not_to(be_valid) }
  end

  describe 'associations' do
    it { is_expected.to(have_many(:users)) }
    it { is_expected.to(have_many(:user_groups)) }
    it { is_expected.to(have_many(:lessons)) }
  end

  describe 'validations' do
    it { is_expected.to(validate_presence_of(:title)) }
  end
end

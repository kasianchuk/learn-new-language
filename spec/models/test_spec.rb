require 'rails_helper'

RSpec.describe(Test, type: :model) do
  context 'associations' do
    it 'belongs to lesson' do
      assc = described_class.reflect_on_association(:lesson)
      expect(assc.macro).to(eq(:belongs_to))
    end

    it 'belongs to user' do
      assc = described_class.reflect_on_association(:user)
      expect(assc.macro).to(eq(:belongs_to))
    end
  end
end

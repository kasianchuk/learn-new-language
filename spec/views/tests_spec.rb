require 'rails_helper'

RSpec.describe 'test/new', type: :view do
  describe 'create test' do
    let(:user) { create(:user) }
    let(:group) { create(:group) }
    let(:word) { create(:word) }
    let(:lesson) { create(:lesson, group_id: group.id) }
    let(:test) { create(:test) }

    it 'new' do
      Lesson.find(lesson.id).words.create(word: 'Cat', description: 'I am a cat',
                                          translation: 'Кіт')
      visit '/tests/new?lesson_id=1'
      fill_in 'user_email', with: user.email
      fill_in 'user_password', with: user.password
      click_button 'commit'
      page.assert_selector('#test', count: 1)
      within('#test') do
        fill_in with: word.translation
      end
      click_button 'commit'
      expect(page).to(have_content('Кіт'))
    end
  end
end

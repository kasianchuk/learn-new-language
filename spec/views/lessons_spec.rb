require 'rails_helper'

describe 'create lesson', type: :views do
  let!(:group) { create(:group) }

  it 'new' do
    visit '/lessons/new'
    within('#lesson') do
      fill_in 'title', with: 'About animals'
      fill_in 'group_id', with: group.id
    end
    click_button 'Create Lesson'
    expect(page).to(have_content('Lesson: About animals'))
  end
end

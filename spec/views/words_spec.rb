require 'rails_helper'

describe 'words actions', type: :views do
  it 'index' do
    Word.create(word: 'cat', description: 'animal')
    Word.create(word: 'banana', description: 'fruit')
    visit '/words'
    expect(page).to(have_content('cat'))
    expect(page).to(have_content('banana'))
  end

  it 'create new word' do
    visit '/words/new'
    within('#create_word') do
      fill_in 'word', with: 'caw'
      fill_in 'description', with: 'animal'
    end
    click_button 'Create Word'
    expect(page).to(have_content('Word: caw Description: animal'))
  end

  it 'destroy' do
    Word.create(word: 'camel', description: 'animal')

    visit '/words'
    click_link 'camel'
    expect(page).to(have_content('Remove'))
    click_link 'Remove'
    expect(page).not_to(have_content('camel'))
  end

  it 'edit word' do
    visit '/words/new'
    within('#create_word') do
      fill_in 'word', with: 'caw'
      fill_in 'description', with: 'animal'
    end
    click_button 'Create Word'
    expect(page).to(have_content('Word: caw Description: animal'))
  end
end

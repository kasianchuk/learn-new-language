require 'rails_helper'

describe 'add word to lesson', type: :views do
  let!(:group) { create(:group) }
  let(:word) { create(:word) }
  let(:lesson) { create(:lesson, group: group) }

  it 'create new lessons_word' do
    visit '/lessons_words/new'
    within('#lessons_word') do
      fill_in 'word_id', with: word.id
      fill_in 'lesson_id', with: lesson.id
    end
    click_button 'Create Lessons word'
    expect(page).to(have_content('added to lesson'))
  end
end

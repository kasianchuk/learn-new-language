require 'rails_helper'

RSpec.describe('Groups', type: :request) do
  let(:user) { create(:user) }
  let(:group) { create(:group) }

  before { sign_in(user) }

  describe 'GET /index' do
    it 'returns http success' do
      get '/groups'
      expect(response).to(have_http_status(:success))
    end
  end

  describe 'GET /show' do
    it 'returns http success' do
      get "/groups/#{group.id}"
      expect(response).to(have_http_status(:success))
    end
  end

  describe 'GET /new' do
    it 'returns http success' do
      get '/groups/new'
      expect(response).to(have_http_status(:success))
    end
  end

  describe 'GET /edit' do
    it 'returns http success' do
      get "/groups/#{group.id}/edit"
      expect(response).to(have_http_status(:success))
    end
  end
end

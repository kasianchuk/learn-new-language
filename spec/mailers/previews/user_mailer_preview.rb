class UserMailerPreview < ActionMailer::Preview
  def new_user_email
    # Set up a temporary lesson for the preview
    group      = Group.new(title: 'Group title')
    lesson     = Lesson.new(title: 'Lesson title', group_id: group.id)
    user       = User.new(first_name: 'Mary', last_name: 'Voitovych', password: '111111')
    UserGroup.new(user_id: user.id, group_id: group.id)

    UserMailer.with(lesson: lesson, user: user).lesson_reminder
  end
end

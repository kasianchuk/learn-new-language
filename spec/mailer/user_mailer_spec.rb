require 'rails_helper'

RSpec.describe(UserMailer, type: :mailer) do
  describe 'mailer' do
    context 'welcome email' do
      let!(:user) do
        User.create(first_name: 'Mariya', last_name: 'Voitovych', email: 'go@gmail.com')
      end
      let!(:mail) { described_class.with(user: user).welcome_email }

      it 'renders the headers' do
        expect(mail.subject).to(eq('Welcome to Learn New Language app'))
        expect(mail.to).to(eq([user.email]))
        expect(mail.from).to(eq(['learn_new_language@example.com']))
      end

      it 'renders the body' do
        expect(mail.body.encoded).to(match('Welcome'))
      end
    end

    context 'delete account' do
      let!(:user) do
        User.create(first_name: 'Mariya', last_name: 'Voitovych', email: 'go@gmail.com')
      end
      let!(:mail) { described_class.with(user: user).delete_account }

      it 'renders the headers' do
        expect(mail.subject).to(eq('Delete account from Learn New Language app'))
        expect(mail.to).to(eq([user.email]))
        expect(mail.from).to(eq(['learn_new_language@example.com']))
      end

      it 'renders the body' do
        expect(mail.body.encoded).to(match('You have deleted your account'))
      end
    end

    context 'lessons reminder email' do
      let!(:user) { create(:user, email: 'test@test.com', password: '111111') }
      let!(:mail) do
        described_class.with(lesson: lesson, user: user).lesson_reminder
      end
      let!(:group) { create(:group) }
      let!(:lesson) { create(:lesson, group_id: group.id) }
      let(:user_from_lesson) { group.users.find_by(id: user.id) }

      before { group.user_groups.create(user: user) }

      it 'renders the headers' do
        expect(mail.subject).to(eq(lesson.title))
        expect(mail.to).to(eq([user.email]))
        expect(mail.from).to(eq(['learn_new_language@example.com']))
      end

      it 'renders the body' do
        expect(mail.body.encoded).to(match('We would like to remind you'))
      end
    end
  end
end

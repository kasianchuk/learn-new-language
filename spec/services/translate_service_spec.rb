require 'rails_helper'

describe TranslateService, vcr: true do
  subject(:translate) { described_class.new(params).translate }

  context 'when translates the word correctly' do
    let(:new_word) { Word.create(word: 'unicorn', description: 'Magic animal') }
    let(:json_body) { '{"responseData": {"translatedText":"Єдиноріг", "match":0.98}}' }
    let(:params) { new_word.id }

    it 'returns translated word' do
      stub_const('TranslateService::URL', Faker::Internet.url)
      allow_any_instance_of(Net::HTTP).to(receive(:request) do
                                            OpenStruct.new(read_body: json_body)
                                          end)
      expect(translate).to(eql('Єдиноріг'))
    end

    it 'adds translated word to translation field' do
      stub_const('TranslateService::URL', Faker::Internet.url)
      allow_any_instance_of(Net::HTTP).to(receive(:request) do
                                            OpenStruct.new(read_body: json_body)
                                          end)
      expect { translate }.to(change do
                                Word.find_by(id: params).translation
                              end.from(nil).to('Єдиноріг'))
    end
  end

  context 'when word_id is nil' do
    let(:params) { nil }

    it 'calls logger error' do
      allow(Rails.logger).to(receive(:error))
      stub_const('TranslateService::URL', Faker::Internet.url)

      translate

      expect(Rails.logger).to(have_received(:error).with("There is no word with id #{params}"))
    end
  end

  context 'when could not translate word' do
    let(:unknown_word) { Word.create(word: 'kriakoziabra', description: 'Who knows what is it?') }
    let(:unknown_word_json_body) do
      '{"responseData": {"translatedText":"kriakoziabra", "match":0.85}}'
    end
    let(:params) { unknown_word.id }

    it 'does not change translation field' do
      allow(Rails.logger).to(receive(:error))
      stub_const('TranslateService::URL', Faker::Internet.url)
      allow_any_instance_of(Net::HTTP).to(receive(:request) do
                                            OpenStruct.new(read_body: unknown_word_json_body)
                                          end)
      expect { translate }.not_to(change(unknown_word, :translation))
    end

    it 'calls logger error' do
      allow(Rails.logger).to(receive(:error))
      stub_const('TranslateService::URL', Faker::Internet.url)
      allow_any_instance_of(Net::HTTP).to(receive(:request) do
                                            OpenStruct.new(read_body: unknown_word_json_body)
                                          end)

      translate

      expect(Rails.logger).to(have_received(:error)
      .with('There is no translation for word "kriakoziabra"'))
    end
  end
end

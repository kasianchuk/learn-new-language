# frozen_string_literal: true

require 'rails_helper'
require 'vcr_configure'

RSpec.describe Connection do
  let(:url) { 'https://twinword-word-graph-dictionary.p.rapidapi.com/definition/?entry=' }

  describe '.call' do
    context 'when valid response' do
      let(:word) { 'language' }
      let(:good_url) { URI([url, word].join) }
      let(:user_response) { VCR.use_cassette('connection') { described_class.call(good_url) } }

      it 'returns valid response' do
        expect(user_response).to be_kind_of(Hash)
      end

      it 'returns the word' do
        expect(user_response['entry']).to eq('language')
      end

      it 'returns lexical category of the word' do
        expect(user_response['meaning'].keys.first).to eq('noun')
      end

      it 'returns the definition of the word' do
        expect(user_response.dig('meaning', 'noun')).to include('communication by word of mouth')
      end

      it 'returns the transcription of the word' do
        expect(user_response['ipa']).to eq('ˈlæŋɡwɪdʒ')
      end
    end

    context 'when invalid response' do
      let(:bad_url_call) { described_class.call(bad_url) }
      let(:wrong_word) { '123' }
      let(:bad_url) { URI([url, wrong_word].join) }
      let(:bad_user_response) { VCR.use_cassette('bad_connection') { bad_url_call } }

      it 'returns the code' do
        expect(bad_user_response['result_code']).to eq('462')
      end

      it 'returns the message' do
        expect(bad_user_response['result_msg']).to eq('Entry word not found')
      end
    end
  end
end

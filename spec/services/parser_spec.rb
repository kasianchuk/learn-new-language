# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Parser do
  let(:raw_data) do
    {
      'definition' =>
                      {
                        'entry'       => 'language',
                        'request'     => 'language',
                        'response'    => 'language',
                        'meaning'     =>
                                         {
                                           'noun'      =>
                                                          '(nou) a systematic means of communicating by the use of sounds or conventional symbols\n(nou) communication by word of mouth\n(nou) the text of a popular song or musical-comedy number\n(nou) the cognitive processes involved in producing and understanding linguistic communication\n(nou) the mental faculty or power of vocal communication\n(nou) a system of words used to name things in a particular discipline',
                                           'verb'      => '',
                                           'adverb'    => '',
                                           'adjective' => ''
                                         },
                        'ipa'         => 'ˈlæŋɡwɪdʒ',
                        'version'     => '7.0.7',
                        'author'      => 'twinword inc.',
                        'email'       => 'help@twinword.com',
                        'result_code' => '200',
                        'result_msg'  => 'Success'
                      }
    }
  end

  let(:parser) { described_class.new(raw_data) }
  let(:keys) { %i[word lexical_category definition ipa association example] }

  describe '#call' do
    it 'has valid format' do
      expect(parser.call).to be_kind_of(Hash)
    end

    it 'has valid keys' do
      expect(parser.call.keys).to eq(keys)
    end
  end

  describe '#word' do
    it do
      expect(parser.word).to eq('language')
    end
  end

  describe '#lexical_category' do
    it do
      expect(parser.lexical_category).to eq('noun')
    end
  end

  describe '#definition' do
    it do
      expect(parser.definition).to include('communication by word of mouth')
    end
  end

  describe '#ipa' do
    it do
      expect(parser.ipa).to eq('ˈlæŋɡwɪdʒ')
    end
  end

  describe '#association' do
    it do
      expect(parser.association).to be_nil
    end
  end

  describe '#example' do
    it do
      expect(parser.example).to be_nil
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'
require 'vcr_configure'

RSpec.describe WordDictionary do
  let(:language) { described_class.new('language').call }
  let(:one_two_three) { described_class.new('123').call }
  let(:user_response) { VCR.use_cassette('word_dictionary') { language } }
  let(:bad_user_response) { VCR.use_cassette('bad_response') { one_two_three } }

  describe '#call' do
    context 'when valid response' do
      it 'returns valid data type' do
        expect(user_response).to be_kind_of(Hash)
      end

      it 'returns the word' do
        expect(user_response[:word]).to eq('language')
      end

      it 'returns lexical category of the word' do
        expect(user_response[:lexical_category]).to eq('noun')
      end

      it 'returns the definition of the word' do
        expect(user_response[:definition]).to include('communication by word of mouth')
      end

      it 'returns the transcription of the word' do
        expect(user_response[:ipa]).to eq('ˈlæŋɡwɪdʒ')
      end
    end

    context 'when invalid response' do
      it 'returns message' do
        expect(bad_user_response.type).to eq('Entry word not found')
      end
    end
  end
end

# frozen_string_literal: true

VCR.configure do |config|
  config.cassette_library_dir     = 'spec/vcr'
  config.default_cassette_options = {
    match_requests_on: %i[method uri body]
  }
  config.ignore_hosts '127.0.0.1', 'localhost'
  config.hook_into :webmock
  config.filter_sensitive_data('<API_KEY>') { Rails.application.credentials.x_rapidapi_key }
end

# frozen_string_literal: true

Rails.application.routes.draw do
  get 'home/home'
  root to: 'home#home'

  devise_for :users
  resources :words
  resources :lessons, only: %i[show index new create]
  resources :lessons_words, only: %i[new create]
  resources :tests, only: %i[new create show]
  resources :groups
  resources :profile, except: %(new create index)
end

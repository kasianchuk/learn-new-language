# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def welcome_email
    @user = params[:user]
    mail(to: @user.email, subject: 'Welcome to Learn New Language app')
  end

  def delete_account
    @user = params[:user]
    mail(to: @user.email, subject: 'Delete account from Learn New Language app')
  end

  def lesson_reminder
    @user            = params[:user]
    @lesson          = params[:lesson]
    @pass_test_url   = 'http://example.com/login'
    @lesson_show_url = 'http://example.com/lesson/id'
    mail(to: @user.email, subject: @lesson.title)
  end
end

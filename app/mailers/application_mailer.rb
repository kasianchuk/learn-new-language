# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'learn_new_language@example.com'
  layout 'mailer'
end

# frozen_string_literal: true

class LessonsController < ApplicationController
  # before_action :authenticate_user!

  def index
    @lessons = Lesson.all
  end

  def new
    @lesson = Lesson.new
  end

  def create
    @group  = Group.find(params.dig(:lesson, :group_id))
    @lesson = @group.lessons.create(lesson_params)
    if @lesson.save
      render status: :created
    else
      render status: :unprocessable_entity, json: @lesson.errors.full_messages
    end
  end

  def show
    @lesson = Lesson.find(params[:id])
  end

  private

  def lesson_params
    params.require(:lesson).permit(:title, :group_id)
  end
end

class TestsController < ApplicationController
  before_action :authenticate_user!

  def new
    @lesson = Lesson.find_by(id: params[:lesson_id])
  end

  def create
    user_translations = params[:user_translations]

    @test = Test.create(lesson_id: params[:lesson_id], user: current_user,
                        user_translations: user_translations,
                        mark: TestService.new(lesson, user_translations).calculate_mark)

    if @test.save
      redirect_to @test, user_translations: params[:user_translations]
    else
      render 'new'
    end
  end

  def show
    @test = Test.find(params[:id])
  end

  def lesson
    @lesson ||= Lesson.find_by(id: params[:lesson_id])
  end
end

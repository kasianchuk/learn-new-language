class ProfilesController < ApplicationController
  before_action :authenticate_user!
  before_action :find_user

  def edit; end

  def update
    if @user.update(user_params)
      format.html { redirect_to @user, notice: 'User was successfully updated.' }
    else
      format.html { render action: 'edit' }
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to user_url }
      format.json { head :no_content }
    end
  end

  def show; end

  private

  def find_user
    @user = current_user
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password)
  end
end

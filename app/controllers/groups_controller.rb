class GroupsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_group, only: %i[show edit update destroy]

  def index
    @groups = Group.all
  end

  def show; end

  def create
    @group = Group.new(group_params)

    if @group.save
      format.html { redirect_to @group, notice: 'Group was successfully created.' }
    else
      format.html { render action: 'new' }
    end
  end

  def new
    @group = Group.new
  end

  def edit; end

  def update
    if @group.update(group_params)
      format.html { redirect_to @group, notice: 'Group was successfully updated.' }
    else
      format.html { render action: 'edit' }
    end
  end

  def destroy
    @group.destroy
    respond_to do |format|
      format.html { redirect_to groups_url }
      format.json { head :no_content }
    end
  end

  private

  def group_params
    params.require(:group).permit(:title)
  end

  def find_group
    @group = Group.find(params[:id])
  end
end

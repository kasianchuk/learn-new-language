# frozen_string_literal: true

class LessonsWordsController < ApplicationController
  def new
    @lessons_word = LessonsWord.new
  end

  def create
    @lessons_word = LessonsWord.create(lessons_words_params)
    if @lessons_word.save
      render status: :created
    else
      render status: :unprocessable_entity, json: @lessons_word.errors.full_messages
    end
  end

  private

  def lessons_words_params
    params.require(:lessons_word).permit(:word_id, :lesson_id)
  end
end

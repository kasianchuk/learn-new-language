# frozen_string_literal: true

class WordsController < ApplicationController
  def index
    @words = Word.all
  end

  def show
    @word = Word.find(params[:id])
  end

  def new
    @word = Word.new
  end

  def create
    @word = Word.create(word_params)
    if @word.save
      render status: :created
    else
      render status: :unprocessable_entity, json: word.errors.full_messages
    end
  end

  def edit
    @word = Word.find(params[:id])
  end

  def update
    @word = Word.find(params[:id])
    if @word.update(word_params)
      redirect_to word_path(@word)
    else
      render status: :unprocessable_entity, json: @word.errors.full_messages
    end
  end

  def destroy
    @word = Word.find(params[:id])
    @word.destroy

    redirect_to words_path
  end

  private

  def word_params
    params.require(:word).permit(:word, :description, :translation, :lesson_id)
  end
end

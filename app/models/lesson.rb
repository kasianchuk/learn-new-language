# frozen_string_literal: true

class Lesson < ApplicationRecord
  has_many :lessons_words, dependent: :destroy
  has_many :words, through: :lessons_words
  has_many :tests, dependent: :destroy
  has_many :users, through: :tests
  belongs_to :group
  validates :title, presence: true
end

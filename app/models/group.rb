# frozen_string_literal: true

class Group < ApplicationRecord
  has_many :user_groups, dependent: nil
  has_many :users, through: :user_groups
  has_many :lessons, dependent: :destroy
  validates :title, presence: true
end

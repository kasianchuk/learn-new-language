# frozen_string_literal: true

class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :user_groups, dependent: nil
  has_many :groups, through: :user_groups
  has_many :tests, dependent: :destroy
  has_many :lessons, through: :test

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true

  enum role: { student: 0, teacher: 1 }
end

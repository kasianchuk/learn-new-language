# frozen_string_literal: true

class Word < ApplicationRecord
  has_many :lessons_words, dependent: :destroy
  has_many :lessons, through: :lessons_words
  validates :word, :description, presence: true
end

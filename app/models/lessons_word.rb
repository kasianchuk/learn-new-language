# frozen_string_literal: true

class LessonsWord < ApplicationRecord
  belongs_to :lesson
  belongs_to :word
end

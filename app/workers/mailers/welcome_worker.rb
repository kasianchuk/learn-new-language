class Mailers
  class WelcomeWorker
    include Sidekiq::Worker

    def perform(user)
      UserMailer.with(user: user).welcome_email
    end
  end
end

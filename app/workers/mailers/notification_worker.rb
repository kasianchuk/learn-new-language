class Mailers
  class NotificationWorker
    include Sidekiq::Worker

    def perform(lesson)
      lesson.users.each do |user|
        UserMailer.with(lesson: lesson, user: user).lesson_reminder
      end
    end
  end
end

class Mailers
  class DeleteWorker
    include Sidekiq::Worker

    def perform(user)
      UserMailer.with(user: user).delete_account
    end
  end
end

# frozen_string_literal: true

require 'json'
require 'uri'
require 'net/http'
require 'openssl'

class Connection
  def self.call(url)
    http = http(url)

    request = request(url)

    response = http.request(request)
    JSON.parse(response.body)
  end

  def self.http(url)
    http             = Net::HTTP.new(url.host, url.port)
    http.use_ssl     = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    http
  end

  def self.request(url)
    request                    = Net::HTTP::Get.new(url)
    request['x-rapidapi-key']  = Rails.application.credentials.x_rapidapi_key
    request['x-rapidapi-host'] = 'twinword-word-graph-dictionary.p.rapidapi.com'
    request
  end

  private_class_method :http, :request
end

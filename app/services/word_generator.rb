# frozen_string_literal: true

require 'spicy-proton'

class WordGenerator
  def noun
    Spicy::Proton.noun
  end

  def adjective
    Spicy::Proton.adjective
  end

  def adverb
    Spicy::Proton.adverb
  end

  def verb
    Spicy::Proton.verb
  end
end

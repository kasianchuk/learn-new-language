# frozen_string_literal: true

require_relative 'word_generator'
require_relative 'connection'
require_relative 'parser'

class WordDictionary
  include ActiveModel::Validations

  # if you want to get additional gategories add [association example] to CATEGORIES
  CATEGORIES = %w[definition].freeze
  URL        = 'https://twinword-word-graph-dictionary.p.rapidapi.com/'
  QUERY      = '/?entry='

  def initialize(word)
    @word = word
  end

  def call
    raw_data = raw_data()
    raw_data['definition'].key?('meaning')
    Parser.new(raw_data).call
  rescue NoMethodError
    errors.add(:base, 'Entry word not found')
  end

  def raw_data
    raw_data = {}

    CATEGORIES.each do |category|
      url                = URI([URL, category, QUERY, @word].join)
      raw_data[category] = Connection.call(url)
    end
    raw_data
  end
end

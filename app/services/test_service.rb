# frozen_string_literal: true

class TestService
  MAX_MARK      = 100

  def initialize(lesson, user_translations)
    @lesson             = lesson
    @user_translations  = user_translations
    @max_word_mark      = (MAX_MARK / lesson.words.count).round
  end

  def calculate_mark
    return MAX_MARK if words_same?

    letters_check
  end

  private

  attr_reader :lesson, :user_translations, :max_word_mark

  def words_same?
    lesson_words.pluck(:translation).map(&:downcase) == user_translations.values.map(&:downcase)
  end

  def letters_check
    mark = 0

    lesson_words.each do |word|
      mark += take_mark(word)
    end
    mark
  end

  def take_mark(word)
    if word.word.chars.map(&:downcase) == user_translations[word.id.to_s].chars.map(&:downcase)
      max_word_mark
    else
      calculate_word_translation(word)
    end
  end

  def calculate_word_translation(word)
    coefficient          = max_word_mark / word.translation.length.to_f
    user_translated_word = user_translations[word.id.to_s]
    word_mark            = 0

    word.translation.chars.each_with_index do |letter, index|
      word_mark += coefficient if equal?(letter, index, user_translated_word)
    end
    word_mark > 5 ? word_mark.round : 0
  end

  def equal?(letter, index, user_translated_word)
    letter.downcase == user_translated_word[index]&.downcase
  end

  def lesson_words
    @lesson_words ||= lesson.words
  end
end

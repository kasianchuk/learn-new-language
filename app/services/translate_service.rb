# frozen_string_literal: true

class TranslateService
  URL = ENV['api_url']

  def initialize(word_id)
    @word_id     = word_id
    @finded_word = Word.find_by(id: word_id)
  end

  def translate
    word_check
  rescue NoMethodError
    Rails.logger.error("There is no word with id #{word_id}")
  end

  private

  attr_reader :word_id, :finded_word

  def url
    @url ||= URI([URL, finded_word.word].join)
  end

  def http
    http             = Net::HTTP.new(url.host, url.port)
    http.use_ssl     = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    http
  end

  def generate_request
    request                    = Net::HTTP::Get.new(url)
    request['x-rapidapi-key']  = ENV['api_key']
    request['x-rapidapi-host'] = ENV['api_host']
    request
  end

  def response
    @response ||= http.request(generate_request).read_body
  end

  def parse_response
    @parse_response ||= JSON.parse(response).dig('responseData', 'translatedText')
  end

  def word_check
    # If there is no translation for word, ex: "kriakoziabra"
    # api returns "kriakoziabra" in translatedText field
    if parse_response == finded_word.word
      Rails.logger.error("There is no translation for word \"#{finded_word.word}\"")
    else
      finded_word.update(translation: parse_response)
      finded_word.translation
    end
  end
end

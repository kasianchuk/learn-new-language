# frozen_string_literal: true

class Parser
  attr_reader :data

  def initialize(data)
    @data = data
  end

  def call
    parsed_data                    = {}
    parsed_data[:word]             = word
    parsed_data[:lexical_category] = lexical_category
    parsed_data[:definition]       = definition
    parsed_data[:ipa]              = ipa
    parsed_data[:association]      = association
    parsed_data[:example]          = example
    parsed_data
  end

  def word
    data.dig('definition', 'entry')
  end

  def lexical_category
    filtered_data(data['definition']['meaning']).keys.first
  end

  def definition
    filtered_data(data['definition']['meaning']).values.first
  end

  def ipa
    data.dig('definition', 'ipa')
  end

  def association
    data.dig('association', 'assoc_word')
  end

  def example
    data.dig('example', 'example')
  end

  private

  def filtered_data(data)
    data.reject { |_key, value| value.empty? }
  end
end
